<?php

/**
 * @file
 * Administration form for the commerce_chronopost module.
 */

/**
 * Form constructor for the Commerce Chronopost settings form.
 */
function commerce_chronopost_settings_form($form, &$form_state) {
  module_load_include('inc', 'commerce_chronopost', 'commerce_chronopost.helpers');
  $form['commerce_chronopost_methods_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Chronopost Services'),
    '#collapsible' => TRUE,
    '#group' => 'groups',
    '#weight' => 0,
  );

  $form['commerce_chronopost_account_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Chronopost account informations'),
    '#collapsible' => TRUE,
    '#group' => 'groups',
    '#weight' => 1,
  );

  $form['commerce_chronopost_shipper_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shipper informations'),
    '#collapsible' => TRUE,
    '#group' => 'groups',
    '#weight' => 2,
  );

  $form['commerce_chronopost_map_presentation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Map presentation options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'groups',
    '#weight' => 3,
  );

  $methods = variable_get('commerce_chronopost_enabled_methods', NULL);
  $form['commerce_chronopost_methods_info']['commerce_chronopost_enabled_methods'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Shipping methodes to enable'),
    '#description' => t('Select the shipping methodes you want to propose to your customers'),
    '#required' => TRUE,
    '#options' => array(
      'chronorelais_shipping_service' => 'ChronoRelais',
      'chrono13_shipping_service' => 'Chrono 13',
      'chronoexpress_shipping_service' => 'Chrono Express',
    ),
    '#default_value' => isset($methods) ? $methods : drupal_map_assoc(array('chronoexpress_shipping_service')),
  );

  $form['commerce_chronopost_account_info']['commerce_chronopost_account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Account Number'),
    '#description' => t('Your Chronopost account number'),
    '#size' => 8,
    '#maxlength' => 8,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_chronopost_account_number', ''),
  );

  $form['commerce_chronopost_account_info']['commerce_chronopost_account_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Account password'),
    '#description' => t('Your Chronopost account password'),
    '#size' => 6,
    '#maxlength' => 6,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_chronopost_account_password', ''),
  );

  $form['commerce_chronopost_account_info']['commerce_chronopost_max_point'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum points'),
    '#description' => t('The maximum number of pickup points to display to the user'),
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => variable_get('commerce_chronopost_max_point', 5),
  );

  $form['commerce_chronopost_account_info']['commerce_chronopost_max_distance'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum distance'),
    '#description' => t('The maximum distance from the user address to look for pickup points'),
    '#size' => 2,
    '#maxlength' => 2,
    '#field_suffix' => t('Km'),
    '#default_value' => variable_get('commerce_chronopost_max_distance', 10),
  );

  $form['commerce_chronopost_shipper_info']['commerce_chronopost_shipper_civility'] = array(
    '#type' => 'radios',
    '#title' => t('Select your civility'),
    '#options' => array(
      'E' => t('Madam'),
      'L' => t('Miss'),
      'M' => t('Mister'),
    ),
    '#default_value' => variable_get('commerce_chronopost_shipper_civility', ''),
    '#required' => TRUE,
  );

  $form['commerce_chronopost_shipper_info']['commerce_chronopost_shipper_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company name'),
    '#default_value' => variable_get('commerce_chronopost_shipper_name', ''),
    '#required' => TRUE,
  );

  $form['commerce_chronopost_shipper_info']['commerce_chronopost_shipper_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#description' => t('The address where you send the package from'),
    '#default_value' => variable_get('commerce_chronopost_shipper_address', ''),
    '#required' => TRUE,
  );

  $form['commerce_chronopost_shipper_info']['commerce_chronopost_shipper_zip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip'),
    '#description' => t('The zip code of the city you send the package from'),
    '#default_value' => variable_get('commerce_chronopost_shipper_zip', ''),
    '#required' => TRUE,
  );

  $form['commerce_chronopost_shipper_info']['commerce_chronopost_shipper_city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#description' => t('The city where you send the package from'),
    '#default_value' => variable_get('commerce_chronopost_shipper_city', ''),
    '#required' => TRUE,
  );

  $form['commerce_chronopost_map_presentation']['commerce_chronopost_map_type'] = array(
    '#type' => 'select',
    '#title' => t('Map type'),
    '#default_value' => variable_get('commerce_chronopost_map_type', 'OSM Mapnik'),
    '#options' => _commerce_chronopost_map_type_options(),
  );
  return system_settings_form($form);
}
