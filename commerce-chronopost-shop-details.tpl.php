<?php
/**
 * @file
 * Template file for the popup above the marker on the leaflet map.
 *
 * Displays information about the pickup point.
 */
?>
<div class="chronopost-shop-detail" id="<?php if (!empty($shop_details['ref'])) : print $shop_details['ref']; endif; ?>">
  <?php if (!empty($shop_details['shop_title'])): ?>
    <h4 class="chronopost-shop-detail-title"><?php print $shop_details['shop_title']; ?></h4>
  <?php endif; ?>
  <?php if (!empty($shop_details['shop_distance'])): ?>
    <span class="chronopost-shop-detail-line"><?php print $shop_details['shop_distance']; ?></span>
  <?php endif; ?>
  <?php if (!empty($shop_details['shop_street'])): ?>
    <span class="chronopost-shop-detail-line"><?php print $shop_details['shop_street']; ?>
  <?php endif; ?>
  <?php if (!empty($shop_details['shop_zip'])): ?>
    <span class="chronopost-shop-detail-line"><?php print $shop_details['shop_zip']; ?></span>
  <?php endif; ?>
    <?php if (!empty($shop_details['shop_locality'])): ?>
      <span class="chronopost-shop-detail-line"><?php print $shop_details['shop_locality']; ?></span>
    <?php endif; ?>
  <?php if (!empty($shop_opening_times)): ?>
    <div class="chronopost-shop-detail-table"><?php print $shop_opening_times; ?></div>
  <?php endif; ?>
  <?php if (!empty($gmap_link)): ?>
    <span class="chronopost-shop-detail-line"><?php print $gmap_link; ?></span>
  <?php endif; ?>
</div>
