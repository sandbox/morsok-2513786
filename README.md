CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
This module provides you with the Chronopost Shipping services for Drupal Commerce.


 * For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/morsok/2513786


 * To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2513786


REQUIREMENTS
------------
This module requires the following modules:
 * Commerce Shipping (https://www.drupal.org/project/commerce_shipping)
 * Leaflet (https://www.drupal.org/project/leaflet)
 * Wsclient Soap (https://www.drupal.org/project/wsclient)


RECOMMENDED MODULES
-------------------
 * Leaflet more maps (https://www.drupal.org/project/leaflet_more_maps):
    When enabled, allows you to choose more map styles for the Chrono Relais map.


INSTALLATION
------------
 * Install all the dependencies.
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * You have to fill out the config form in order to use the module. Go to admin/commerce/config/chronopost.


MAINTAINERS
-----------
Current maintainers:
 * Alexandre Amiche (morsok) - https://www.drupal.org/u/morsok


This project has been sponsored by:
 * Eurelis (https://www.drupal.org/node/2508117)
   Visit http://www.eurelis.com/ for more information.