<?php

/**
 * @file
 * Theme callbacks for commerce_chronopost.
 */

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_commerce_chronopost_shop_details(&$vars) {
  module_load_include('inc', 'commerce_chronopost', 'commerce_chronopost.helpers');
  drupal_add_css(drupal_get_path('module', 'commerce_chronopost') . '/css/commerce_chronopost.css');
  $rows = array();
  foreach ($vars['shop_details']['shop_opening_times'] as $day_int => $info) {
    $day = _commerce_chronopost_int_to_day($day_int);
    $rows[$day]['day'] = $day;
    $info_split = explode(' ', $info);
    $rows[$day]['info_morning'] = $info_split[0];
    if (isset($info_split[1])) {
      $rows[$day]['info_afternoon'] = $info_split[1];
    }
  }
  $vars['shop_opening_times'] = theme('table',
    array(
      'header' => array(),
      'rows' => $rows,
      'attributes' => array('class' => array('commerce-chronopost-shop-timetable')),
    )
  );
}
