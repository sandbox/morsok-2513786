<?php

/**
 * @file
 * Helper functions for the commerce_chronopost module.
 */

/**
 * Helper function to populate the map type select on the admin form.
 *
 * @return array
 *   An array of map type options for a select form element.
 */
function _commerce_chronopost_map_type_options() {
  $map_options = array();
  foreach (leaflet_map_get_info() as $key => $map) {
    $map_options[$key] = html_entity_decode(t('@label', array('@label' => $map['label'])));
  }

  return $map_options;
}

/**
 * Helper function to translate the soap day from number to string.
 *
 * @param int $number
 *   The day of the week in a number form.
 *
 * @return string
 *   The day in a string form.
 */
function _commerce_chronopost_int_to_day($number) {
  $week = array(
    '1' => t('Monday'),
    '2' => t('Tuesday'),
    '3' => t('Wednesday'),
    '4' => t('Thursday'),
    '5' => t('Friday'),
    '6' => t('Saturday'),
    '7' => t('Sunday'),
  );
  return $week[$number];
}

/**
 * Helper function to return a service object.
 *
 * The service object allows us to make a SOAP call to the Chronopost API.
 *
 * @param string $type
 *   The type of service requested.
 *
 * @return bool|\WSClientServiceDescription The service object or false if there was an error.
 *    The service object or false if there was an error.
 */
function _commerce_chronopost_get_service($type) {
  $service = new WSClientServiceDescription();
  switch ($type) {
    case 'point_relais':
      $service->url = 'https://www.chronopost.fr/recherchebt-ws-cxf/PointRelaisServiceWS?wsdl';
      break;

    case 'etiquettes':
      $service->url = 'https://www.chronopost.fr/shipping-cxf/ShippingServiceWS?wsdl';
      break;
  }
  $service->type = 'soap';
  try {
    $service->endpoint()->initializeMetaData();
  }
  catch (WSClientException $e) {
    watchdog_exception('SOAP', $e);
    return FALSE;
  }
  return $service;
}

/**
 * Helper function to return the Chronopost product code.
 *
 * The product code is based on the shipping service selected by the user.
 *
 * @param object $order
 *   The commerce order.
 *
 * @return string
 *   The product code or ? if not known.
 */
function _commerce_chronopost_get_product_code($order) {
  foreach ($order->commerce_line_items[LANGUAGE_NONE] as $key => $line_item_id) {
    $line_item = commerce_line_item_load($line_item_id['line_item_id']);
    if ($line_item && $line_item->type == 'shipping' && $line_item->data && $line_item->data['shipping_service']['shipping_method'] == 'chronopost_shipping_method') {
      switch ($line_item->data['shipping_service']['base']) {
        case 'chronorelais_shipping_service':
          return '86';

        case 'chrono13_shipping_service':
          return '01';

        case 'chronoexpress_shipping_service':
          return '17';
      }
    }
  }
  return '?';
}

/**
 * Prepare the recipient information for the etiquette generation.
 *
 * The informations are based on the shipping service.
 *
 * @param object $order
 *   The order object.
 * @param object $shipping_profile
 *   The shipping profile object.
 * @param string $product_code
 *   The Chronopost product code.
 *
 * @return array
 *   The recipient informations.
 */
function _commerce_chronopost_get_recipient_value($order, $shipping_profile, $product_code) {
  $recipient = array();
  switch ($product_code) {
    case '86':
      foreach ($order->commerce_line_items[LANGUAGE_NONE] as $key => $line_item_id) {
        $line_item = commerce_line_item_load($line_item_id['line_item_id']);
        if ($line_item && $line_item->type == 'shipping' && $line_item->data && $line_item->data['shipping_service']['shipping_method'] == 'chronopost_shipping_method') {
          if ($line_item->data['shipping_service']['base'] == 'chronorelais_shipping_service') {
            module_load_include('inc', 'commerce_chronopost', 'commerce_chronopost.soap');
            $ref = $line_item->data['service_details']['ref_relais'];
            $shop_infos = commerce_chronopost_get_shop($ref);
            $shop_infos = reset($shop_infos);
          }
        }
      }
      if (isset($shop_infos)) {
        $recipient['recipientName'] = $shop_infos['details']['shop_title'];
        $recipient['recipientName2'] = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0]['name_line'];
        $recipient['recipientAdress1'] = $shop_infos['details']['shop_street'];
        $recipient['recipientAdress2'] = $shop_infos['details']['shop_street_2'];
        $recipient['recipientZipCode'] = $shop_infos['details']['shop_zip'];
        $recipient['recipientCity'] = $shop_infos['details']['shop_locality'];
        $recipient['recipientCountry'] = $shop_infos['details']['shop_country'];
      }
      break;

    default:
      $recipient['recipientName'] = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0]['name_line'];
      $recipient['recipientName2'] = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0]['organisation_name'];
      $recipient['recipientAdress1'] = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0]['thoroughfare'];
      $recipient['recipientAdress2'] = '';
      $recipient['recipientZipCode'] = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0]['postal_code'];
      $recipient['recipientCity'] = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0]['locality'];
      $recipient['recipientCountry'] = $shipping_profile->commerce_customer_address[LANGUAGE_NONE][0]['country'];
      break;
  }
  $recipient['recipientPreAlert'] = 0;
  return $recipient;
}

/**
 * Compute the order total weight.
 *
 * @param object $order
 *   The commerce order.
 *
 * @return int
 *   The total weight.
 */
function _commerce_chronopost_get_order_weight($order) {
  $weight = 0;
  $line_items = $order->commerce_line_items[LANGUAGE_NONE];
  foreach ($line_items as $key => $line_item) {
    $item = commerce_line_item_load($line_item['line_item_id']);
    if ($item->type == 'product') {
      $product = commerce_product_load($item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      if (isset($product->field_weight)) {
        $weight += $product->field_weight[LANGUAGE_NONE][0]['value'] * $item->quantity;
      }
    }
  }
  return $weight;
}
