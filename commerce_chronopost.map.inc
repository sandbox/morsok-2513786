<?php

/**
 * @file
 * All map related function of the Commerce Chronopost module.
 */

/**
 * Retrieves the pickup points given the customer address and returns a map.
 *
 * @param array $shops
 *   The pickup points available in the area.
 *
 * @return string $map
 *    The map html.
 */
function commerce_chronopost_get_map(array $shops) {
  $features = array();
  foreach ($shops as $label => $shop) {
    $features[] = array(
      'type' => 'point',
      'lat' => $shop['lat'],
      'lon' => $shop['lon'],
      'icon' => array(
        'iconUrl' => url(drupal_get_path('module', 'commerce_chronopost') . '/images/chronopost_marker.png', array(
          'absolute' => TRUE,
          'language' => (object) array('language' => FALSE),
        )),
        'className' => 'chronorelais-marker',
      ),
      'popup' => theme('commerce_chronopost_shop_details', array(
        'shop_details' => $shop['details'],
        'gmap_link' => $shop['link'],
      )),
      'label' => $label,
    );
  }
  $map_types = leaflet_map_get_info();
  $map_type = variable_get('commerce_chronopost_map_type');
  $map = leaflet_build_map($map_types[$map_type], $features);
  return $map;
}
