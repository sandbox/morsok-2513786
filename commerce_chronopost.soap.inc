<?php

/**
 * @file
 * SOAP related function for the Chronopost module.
 */

/**
 * Returns a list of pickup points near the custom address.
 *
 * @param array $address
 *   The customer address as given on the shipping profile.
 * @param int $weight
 *   The total weight of the products to send in grammes.
 *
 * @return array $shops
 *   An array of pickup points formatted for the map display.
 */
function commerce_chronopost_get_shops(array $address, $weight) {
  $shops = array();
  module_load_include('inc', 'commerce_chronopost', 'commerce_chronopost.helpers');
  $service = _commerce_chronopost_get_service('point_relais');
  if ($service) {
    $shops = array();
    $date = new DateTime('tomorrow');
    $date = $date->format('d/m/Y');
    $args = array(
      'accountNumber' => variable_get('commerce_chronopost_account_number'),
      'password' => variable_get('commerce_chronopost_account_password'),
      'adress' => $address['thoroughfare'],
      'zipCode' => $address['postal_code'],
      'city' => $address['locality'],
      'countryCode' => $address['country'],
      'type' => 'P',
      'service' => 'T',
      'weight' => $weight,
      'shippingDate' => $date,
      'maxPointChronopost' => variable_get('commerce_chronopost_max_point'),
      'maxDistanceSearch' => variable_get('commerce_chronopost_max_distance'),
      'holidayTolerant' => 1,
    );
    $result = $service->recherchePointChronopost($args);
    if ($result && isset($result->return)) {
      $result = $result->return;
      if ($result->errorCode == 0) {
        $pickups_points = $result->listePointRelais;
        foreach ($pickups_points as $key => $shop) {
          $shops[$shop->identifiant] = array(
            'lat' => $shop->coordGeolocalisationLatitude,
            'lon' => $shop->coordGeolocalisationLongitude,
            'details' => array(
              'ref' => $shop->identifiant,
              'shop_title' => $shop->nom,
              'shop_street' => $shop->adresse1,
              'shop_zip' => $shop->codePostal,
              'shop_opening_times' => array(),
            ),
            'link' => l(t('See on google maps'), $shop->urlGoogleMaps, array('attributes' => array('target' => '_blank'))),
          );
          if ($shop->distanceEnMetre > 0) {
            $shops[$shop->identifiant]['details']['shop_distance'] = $shop->distanceEnMetre . 'm';
          }
          foreach ($shop->listeHoraireOuverture as $key_jour => $jour) {
            if (isset($jour->horairesAsString)) {
              $shops[$shop->identifiant]['details']['shop_opening_times'][$jour->jour] = $jour->horairesAsString;
            }
          }
          ksort($shops[$shop->identifiant]['details']['shop_opening_times']);
        }
      }
    }
  }
  return $shops;
}

/**
 * Returns the details about a specific pickup point.
 *
 * @param string $ref
 *   The SHOP ID.
 *
 * @return array
 *   The pickup point informations.
 */
function commerce_chronopost_get_shop($ref) {
  module_load_include('inc', 'commerce_chronopost', 'commerce_chronopost.helpers');
  $pickup_point = array();
  $service = _commerce_chronopost_get_service('point_relais');
  if ($service) {
    $args = array(
      'accountNumber' => variable_get('commerce_chronopost_account_number'),
      'password' => variable_get('commerce_chronopost_account_password'),
      'identifiant' => $ref,
    );
    $result = $service->rechercheDetailPointChronopost($args);
    if ($result && isset($result->return)) {
      $result = $result->return;
      if ($result->errorCode == 0) {
        $shop = $result->listePointRelais;
        $pickup_point[$shop->identifiant] = array(
          'lat' => $shop->coordGeolocalisationLatitude,
          'lon' => $shop->coordGeolocalisationLongitude,
          'details' => array(
            'ref' => $shop->identifiant,
            'shop_title' => $shop->nom,
            'shop_street' => $shop->adresse1,
            'shop_street_2' => $shop->adresse2,
            'shop_locality' => $shop->localite,
            'shop_country' => $shop->codePays,
            'shop_zip' => $shop->codePostal,
            'shop_opening_times' => array(),
          ),
          'link' => l(t('See on google maps'), $shop->urlGoogleMaps, array('attributes' => array('target' => '_blank'))),
        );
        if ($shop->distanceEnMetre > 0) {
          $pickup_point[$shop->identifiant]['details']['shop_distance'] = $shop->distanceEnMetre . 'm';
        }
        foreach ($shop->listeHoraireOuverture as $key_jour => $jour) {
          if (isset($jour->horairesAsString)) {
            $pickup_point[$shop->identifiant]['details']['shop_opening_times'][$jour->jour] = $jour->horairesAsString;
          }
        }
        ksort($pickup_point[$shop->identifiant]['details']['shop_opening_times']);
      }
    }
  }
  return $pickup_point;
}

/**
 * Sends to the browser the etiquette.
 *
 * This function use the Chronopost SOAP API to retrieve a PDF etiquette needed
 * to send the parcel.
 *
 * @param object $order
 *   The commerce order.
 */
function commerce_chronopost_get_etiquette($order) {
  $billing_profile = commerce_customer_profile_load($order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id']);
  $shipping_profile = commerce_customer_profile_load($order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']);
  module_load_include('inc', 'commerce_chronopost', 'commerce_chronopost.helpers');
  $service = _commerce_chronopost_get_service('etiquettes');
  if ($service) {
    $product_code = _commerce_chronopost_get_product_code($order);
    $args = array(
      'headerValue' => array(
        'idEmit' => 'CHRFR',
        'accountNumber' => variable_get('commerce_chronopost_account_number'),
        'subAccount' => '',
      ),
      'shipperValue' => array(
        'shipperCivility' => variable_get('commerce_chronopost_shipper_civility'),
        'shipperName' => variable_get('commerce_chronopost_shipper_name'),
        'shipperName2' => '',
        'shipperAdress1' => variable_get('commerce_chronopost_shipper_address'),
        'shipperAdress2' => '',
        'shipperZipCode' => variable_get('commerce_chronopost_shipper_zip'),
        'shipperCity' => variable_get('commerce_chronopost_shipper_city'),
        'shipperCountry' => 'FR',
        'shipperContactName' => '',
        'shipperEmail' => '',
        'shipperPhone' => '',
        'shipperMobilePhone' => '',
        'shipperPreAlert' => 0,
      ),
      'customerValue' => array(
        'customerCivility' => '?',
        'customerName' => $billing_profile->commerce_customer_address[LANGUAGE_NONE][0]['name_line'],
        'customerName2' => $billing_profile->commerce_customer_address[LANGUAGE_NONE][0]['organisation_name'],
        'customerAdress1' => $billing_profile->commerce_customer_address[LANGUAGE_NONE][0]['thoroughfare'],
        'customerAdress2' => '',
        'customerZipCode' => $billing_profile->commerce_customer_address[LANGUAGE_NONE][0]['postal_code'],
        'customerCity' => $billing_profile->commerce_customer_address[LANGUAGE_NONE][0]['locality'],
        'customerCountry' => $billing_profile->commerce_customer_address[LANGUAGE_NONE][0]['country'],
        'customerPreAlert' => 0,
      ),
      'refValue' => array(),
      'skybillValue' => array(
        'evtCode' => 'DC',
        'productCode' => $product_code,
        'shipHour' => date('H'),
        // Service 0 => Normal.
        'service' => '0',
        'objectType' => 'MAR',
        'weight' => _commerce_chronopost_get_order_weight($order),
        'weightUnit' => 'KGM',
      ),
      'skybillParamsValue' => array(),
      'password' => variable_get('commerce_chronopost_account_password'),
    );
    $args['recipientValue'] = _commerce_chronopost_get_recipient_value($order, $shipping_profile, $product_code);
    try {
      $result = $service->shipping($args);
      if ($result && isset($result->return)) {
        $result = $result->return;
        if ($result->errorCode == 0) {
          $file = file_save_data($result->skybill, "temporary://");
          $headers = file_get_content_headers($file);
          $headers['Content-Disposition'] = 'attachment; filename=' . $result->skybillNumber . '.pdf';
          file_transfer($file->uri, $headers);
        }
      }
      drupal_set_message(t('There was an error while generating the etiquette, please see the logs for more informations.'), 'error');
      watchdog('commerce_chronopost', 'Error while generating the etiquette : @error', array('@error' => $result->errorMessage), WATCHDOG_ERROR);
      drupal_goto(drupal_get_destination());
    }
    catch (WSClientException $e) {
      watchdog_exception('SOAP', $e);
      drupal_set_message(t('There was an error while generating the etiquette, please see the logs for more informations.'), 'error');
      drupal_goto(drupal_get_destination());
    }
  }
}
