(function ($) {
  Drupal.behaviors.commerceChronoRelais = {
    attach: function (context, settings) {
      $('.chronorelais-marker').click(function() {
        var ref = $(this).attr('title');
        $('#ref-relais').val(ref);
        var selected_html = $('#' + ref).html();
        var sentence = Drupal.t("<p>You selected the following pickup point :</p>");
        $('#edit-commerce-shipping-service-details-selected').html(sentence + selected_html);
      });
    }
  };
})(jQuery);
